export default class SwapiService {
    _baseApiUrl = 'https://swapi.dev/api';
    _imgBase = 'https://starwars-visualguide.com/assets/img';

    _extractId(url) {
        const idRegExp = /\/([0-9]*)\/$/;
        return url.match(idRegExp)[1];
    }

    _transformPlanet = ({name, population, rotation_period: rotationPeriod, diameter, url}) => {
        return {
            id: this._extractId(url),
            name,
            population,
            rotationPeriod,
            diameter
        }
    }

    _transformStarship = ({
                              name,
                              model,
                              manufacturer,
                              cost_in_credits: costInCredits,
                              length,
                              crew,
                              passengers,
                              cargo_capacity: cargoCapacity,
                              url
                          }) => {
        return {
            id: this._extractId(url),
            name,
            model,
            manufacturer,
            costInCredits,
            length,
            crew,
            passengers,
            cargoCapacity
        }
    }

    _transformPerson = ({name, gender, birth_year: birthYear, eye_color: eyeColor, url}) => {
        return {
            id: this._extractId(url),
            name,
            gender,
            birthYear,
            eyeColor
        }
    }

    async getResource(url) {
        const res = await fetch(`${this._baseApiUrl}${url}`);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}, received ${res.status}`)
        }
        return await res.json();
    }

    getPeople = async () => {
        const result = await this.getResource('/people');
        return result.results.map(this._transformPerson);
    }

    getPerson = async (id) => {
        const person = await this.getResource(`/people/${id}`);
        return this._transformPerson(person);
    }

    getPlanets = async () => {
        const result = await this.getResource('/planets');
        return result.results.map(this._transformPlanet);
    }

    getPlanet = async (id) => {
        const planet = await this.getResource(`/planets/${id}`);
        return this._transformPlanet(planet);
    }

    getStarships = async () => {
        const result = await this.getResource('/starships');
        return result.results.map(this._transformStarship);
    }

    getStarship = async (id) => {
        const starship = await this.getResource(`/starships/${id}`);
        return this._transformStarship(starship);
    }

    getPersonImg = (id) => `${this._imgBase}/characters/${id}.jpg`;

    getPlanetImg = (id) => `${this._imgBase}/planets/${id}.jpg`;

    getStarshipImg = (id) => `${this._imgBase}/starships/${id}.jpg`;
}