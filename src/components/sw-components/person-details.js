import React from 'react';
import ItemDetails from '../item-details';
import {Record} from '../item-details/item-details';
import ErrorBoundary from '../error-boundary';
import {withApiService} from '../hoc-helpers';

const PersonDetails = (props) => {
    return (
        <ErrorBoundary>
            <ItemDetails {...props}>
                <Record field='gender' label='Gender'/>
                <Record field='birthYear' label='Birth Year'/>
                <Record field='eyeColor' label='Eye Color'/>
            </ItemDetails>
        </ErrorBoundary>
    );
}

const mapMethodsToProps = ({getPerson: getData, getPersonImg: getImage}) => ({
    getData, getImage
})

export default withApiService(mapMethodsToProps)(PersonDetails);