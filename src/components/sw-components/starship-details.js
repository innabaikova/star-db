import React from 'react';
import ItemDetails from '../item-details';
import {Record} from '../item-details/item-details';
import ErrorBoundary from '../error-boundary';
import {withApiService} from '../hoc-helpers';

const StarshipDetails = (props) => {
    return (
        <ErrorBoundary>
            <ItemDetails {...props}>
                <Record field='model' label='Model'/>
                <Record field='manufacturer' label='Manufacturer'/>
                <Record field='costInCredits' label='Cost In Credits'/>
                <Record field='length' label='Length'/>
                <Record field='crew' label='Crew'/>
                <Record field='passengers' label='Passengers'/>
                <Record field='cargoCapacity' label='Cargo Capacity'/>
            </ItemDetails>
        </ErrorBoundary>
    )
}

const mapMethodsToProps = ({getStarship: getData, getStarshipImg: getImage}) => ({
    getData, getImage
})

export default withApiService(mapMethodsToProps)(StarshipDetails);