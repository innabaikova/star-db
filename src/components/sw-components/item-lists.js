import {compose, withApiService, withData, withRenderFn} from '../hoc-helpers';
import ItemsList from '../items-list';

const renderPerson = i => `${i.name} (${i.gender}, ${i.birthYear})`;
const renderPlanet = i => `${i.name} (${i.diameter}km)`;
const renderStarship = i => `${i.name} (${i.manufacturer})`;

const mapPersonMethodsToProps = ({getPeople: getData}) => ({getData});
const mapPlanetMethodsToProps = ({getPlanets: getData}) => ({getData});
const mapStarshipMethodsToProps = ({getStarships: getData}) => ({getData});

const PeopleList = compose(
    withApiService(mapPersonMethodsToProps),
    withData,
    withRenderFn(renderPerson)
)(ItemsList)
const PlanetsList = compose(
    withApiService(mapPlanetMethodsToProps),
    withData,
    withRenderFn(renderPlanet)
)(ItemsList)
const StarshipsList = compose(
    withApiService(mapStarshipMethodsToProps),
    withData,
    withRenderFn(renderStarship)
)(ItemsList)

export {
    PeopleList,
    PlanetsList,
    StarshipsList
}