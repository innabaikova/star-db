import React from 'react';
import ItemDetails from '../item-details';
import {Record} from '../item-details/item-details';
import ErrorBoundary from '../error-boundary';
import {withApiService} from '../hoc-helpers';

const PlanetDetails = (props) => {
    return (
        <ErrorBoundary>
            <ItemDetails {...props}>
                <Record field='population' label='Population'/>
                <Record field='rotationPeriod' label='Rotation Period'/>
                <Record field='diameter' label='Diameter'/>
            </ItemDetails>
        </ErrorBoundary>
    );
}

const mapMethodsToProps = ({getPlanet: getData, getPlanetImg: getImage}) => ({
    getData, getImage
});

export default withApiService(mapMethodsToProps)(PlanetDetails);