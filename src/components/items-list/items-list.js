import React from 'react';
import './items-list.css';
import PropTypes from 'prop-types';

const ItemsList = (props) => {
    const renderItems = () => {
        const {items, onItemSelected, children: renderItem} = props;
        return items.map(item => {
            const label = renderItem(item);

            return (
                <li className='list-group-item' key={item.id}
                    onClick={() => onItemSelected(item.id)}>
                    {label}
                </li>
            );
        })
    }

    return (
        <ul className='item-list list-group'>
            {renderItems()}
        </ul>
    );
}

ItemsList.defaultProps = {
    onItemSelected: () => {}
}

ItemsList.propTypes = {
    onItemSelected: PropTypes.func,
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    children: PropTypes.func.isRequired
}

export default ItemsList;