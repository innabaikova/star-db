const compose = (...functions) => (component) => {
    return functions.reduceRight((previousValue, fn) => fn(previousValue), component);
}

export default compose;