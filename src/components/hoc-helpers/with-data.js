import React, {useEffect, useState} from 'react';
import Spinner from '../spinner';

export const withData = (View) => {
    return (props) => {
        const [items, setItems] = useState(null);
        const {getData} = props;

        useEffect(() => {
            getData().then(setItems)
        }, [getData]);

        if (!items) {
            return (<Spinner/>);
        }

        return (<View {...props} items={items}/>);
    }
}