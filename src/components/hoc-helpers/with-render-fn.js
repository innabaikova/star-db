import React from 'react';

export const withRenderFn = (renderFn) => (Wrapped) => {
    return (props) => {
        return (
            <Wrapped {...props}>
                {renderFn}
            </Wrapped>
        )
    }
};