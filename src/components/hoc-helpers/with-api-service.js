import React, {useContext} from 'react';
import SwapiServiceContext from '../swapi-service-context';

export const withApiService = (mapMethodsToProps) => (Wrapped) => {
    return (props) => {
        const apiService = useContext(SwapiServiceContext);
        const serviceProps = mapMethodsToProps(apiService);

        return (<Wrapped {...props} {...serviceProps} />);
    };
};