import {withData} from './with-data';
import {withApiService} from './with-api-service'
import {withRenderFn} from './with-render-fn'
import compose from './compose'

export {withData, withApiService, withRenderFn, compose};