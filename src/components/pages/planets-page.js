import React, {useState} from 'react';
import Row from '../row/row';
import ErrorBoundary from '../error-boundary';
import { PlanetsList, PlanetDetails} from '../sw-components';

export const PlanetsPage = () => {
    const [selectedPlanet, selectPlanet] = useState(null);

    return (
        <ErrorBoundary>
            <Row left={<PlanetsList onItemSelected={selectPlanet}/>}
                 right={selectedPlanet && <PlanetDetails id={selectedPlanet}/>}/>
        </ErrorBoundary>
    );
}