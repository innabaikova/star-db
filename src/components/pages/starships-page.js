import React from 'react';
import { StarshipsList } from '../sw-components';
import {useHistory} from 'react-router-dom';

export const StarshipsPage = () => {
    const history = useHistory();
    const selectStarship = (id) => history.push(`/starships/${id}`);

    return (
        <StarshipsList onItemSelected={selectStarship}/>
    );
}