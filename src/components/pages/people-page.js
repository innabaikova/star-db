import React from 'react';
import Row from '../row/row';
import ErrorBoundary from '../error-boundary';
import {PeopleList, PersonDetails} from '../sw-components';
import {withRouter} from 'react-router-dom';

const PeoplePage = ({history, match}) => {
    const {id} = match.params;
    const selectPerson = (id) => history.push(`/people/${id}`);

    return (
        <ErrorBoundary>
            <Row left={<PeopleList onItemSelected={selectPerson}/>}
                 right={id && <PersonDetails id={id}/>}/>
        </ErrorBoundary>
    );
}

export default withRouter(PeoplePage);