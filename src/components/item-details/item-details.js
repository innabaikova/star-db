import React, {useEffect, useState, cloneElement, Children} from 'react';
import './item-details.css';
import Spinner from '../spinner';
import ErrorButton from '../error-button';

const Record = ({item, field, label}) => {
    return (
        <li className='list-group-item'>
            <span className='term'>{label}</span>
            <span>{item[field]}</span>
        </li>
    );
}

export {
    Record
}

export const ItemDetails = ({id, getData, getImage, children: records}) => {
    const [item, setItem] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);

        getData(id).then((item) => {
            setItem(item);
            setLoading(false);
        });
    }, [id, getData]);

    return (
        <div className='item-details card'>
            {loading && <Spinner/>}
            {!loading && item && <ItemView records={records} item={item} img={getImage(id)}/>}
        </div>
    );
}

const ItemView = ({records, item, img}) => {
    const {name} = item;

    return (
        <React.Fragment>
            <img className='item-image'
                 src={img}
                 alt='details'/>

            <div className='card-body'>
                <div className='d-flex'>
                    <h4>{name}</h4>
                    <ErrorButton/>
                </div>
                <ul className='list-group list-group-flush'>
                    {
                        Children.map(records, (record) =>
                            cloneElement(record, {item})
                        )
                    }
                </ul>
            </div>
        </React.Fragment>
    );
}