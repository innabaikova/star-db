import React from 'react';
import Header from '../header';
import RandomPlanet from '../random-planet';
import './app.css';
import {PeoplePage, PlanetsPage, StarshipsPage} from '../pages';
import ErrorBoundary from '../error-boundary';
import SwapiServiceContext from '../swapi-service-context';
import SwapiService from '../../sevices/swapi-service';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import StarshipDetails from '../sw-components/starship-details';

export const App = () => {
    const apiService = new SwapiService();

    return (
        <ErrorBoundary>
            <SwapiServiceContext.Provider value={apiService}>
                <Router>
                    <div className='container'>
                        <Header/>
                        <RandomPlanet/>

                        <Switch>
                            <Route path='/' exact/>
                            <Route path='/people/:id?' component={PeoplePage}/>
                            <Route path='/planets' component={PlanetsPage}/>
                            <Route path='/starships' component={StarshipsPage} exact/>
                            <Route path='/starships/:id' render={({match}) =>
                                <StarshipDetails id={match.params.id}/>
                            }/>
                            <Redirect to='/' />
                        </Switch>
                    </div>
                </Router>
            </SwapiServiceContext.Provider>
        </ErrorBoundary>
    );
}