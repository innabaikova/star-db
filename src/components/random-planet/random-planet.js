import React, {useCallback, useEffect, useState} from 'react';
import './random-planet.css';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';
import {withApiService} from '../hoc-helpers';
import PropTypes from 'prop-types';

const RandomPlanet = ({updateInterval, getData, getImage}) => {
    const [planet, setPlanet] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    const onPlanetLoaded = (planet) => {
        setPlanet(planet);
        setLoading(false);
    };

    const onError = () => {
        setError(true);
        setLoading(false)
    };

    const updatePlanet = useCallback(() => {
        const id = Math.floor(Math.random() * 10) + 2;
        getData(id).then(onPlanetLoaded).catch(onError);
    }, [getData]);

    useEffect(() => {
        updatePlanet();
        const interval = setInterval(updatePlanet, updateInterval);
        return () => clearInterval(interval);
    }, [updatePlanet, updateInterval]);

    return (
        <div className='random-planet jumbotron rounded'>
            {loading && <Spinner/>}
            {!(loading || error) && <PlanetView planet={planet} img={getImage(planet.id)}/>}
            {error && <ErrorIndicator/>}
        </div>
    );
}

RandomPlanet.defaultProps = {
    updateInterval: 10000
};

RandomPlanet.propTypes = {
    updateInterval: PropTypes.number
}

const PlanetView = ({planet, img}) => {
    const {name, population, rotationPeriod, diameter} = planet;

    return (
        <React.Fragment>
            <img className='planet-image'
                 src={img}
                 alt='planet'/>
            <div>
                <h4>{name}</h4>
                <ul className='list-group list-group-flush'>
                    <li className='list-group-item'>
                        <span className='term'>Population</span>
                        <span>{population}</span>
                    </li>
                    <li className='list-group-item'>
                        <span className='term'>Rotation Period</span>
                        <span>{rotationPeriod}</span>
                    </li>
                    <li className='list-group-item'>
                        <span className='term'>Diameter</span>
                        <span>{diameter}</span>
                    </li>
                </ul>
            </div>
        </React.Fragment>
    );
}

const mapMethodsToProps = ({getPlanet: getData, getPlanetImg: getImage}) => ({
    getData, getImage
})

export default withApiService(mapMethodsToProps)(RandomPlanet);