import React, {useState} from 'react';
import './error-button.css';

export const ErrorButton = () => {
    const [renderError, setError] = useState(false);

    if (renderError) {
        this.foo.bar = 0;
    }

    return (
        <button
            className='error-button btn btn-danger btn-lg'
            onClick={() => setError(true)}>
            Throw Error
        </button>
    );
}